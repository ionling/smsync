# frozen_string_literal: true

require 'json'

require 'aws-sdk-s3'
require 'http'
require 'toml-rb'

class SMMS
  def initialize(token)
    @client = HTTP.auth(token)
  end

  def list
    resp = @client.get('https://sm.ms/api/v2/upload_history')
    JSON.parse(resp.to_s)['data']
  end
end

class MinIO
  def initialize(endpoint, access_key, secret_key, bucket)
    @client = Aws::S3::Client.new(
      endpoint: endpoint,
      access_key_id: access_key,
      secret_access_key: secret_key,
      force_path_style: true
    )
    @bucket = bucket
  end

  def put(opts)
    opts[:bucket] = @bucket
    @client.put_object(opts)
  end

  def delete(key)
    @client.delete_object(bucket: @bucket, key: key)
  end

  def list
    return to_enum(__method__) unless block_given?

    r = @client.list_objects_v2({ bucket: @bucket })
    r.contents.each { |x| yield x }
    while r.is_truncated
      token = r.next_continuation_token
      r = @client.list_objects_v2(
        bucket: @bucket,
        continuation_token: token
      )
      r.contents.each { |x| yield x }
    end
  end

  def object_summary(key)
    Aws::S3::ObjectSummary.new(
      bucket_name: @bucket,
      key: key,
      client: @client
    )
  end
end

class App
  def initialize(config_file)
    config = TomlRB.load_file(config_file)
    @sm = SMMS.new(config['smms']['token'])
    c = config['minio']
    @mio = MinIO.new(c['endpoint'], c['access_key'], c['secret_key'], c['bucket'])
  end

  def backup
    @sm.list.each do |x|
      check_and_backup x
    end
  end

  def diff
    sm_names = @sm.list.collect { |x| x['path'] }.to_set
    mio_names = @mio.list.collect { |x| '/' + x.key }.to_set
    sm_only = sm_names - mio_names
    mio_only = mio_names - sm_names
    puts 'SM only', sm_only unless sm_only.empty?
    puts 'MinIO only', mio_only unless mio_only.empty?
    puts 'No difference' if sm_only.empty? && mio_only.empty?
  end

  # Sync SM.MS to MinIO.
  # This will remove redundant objects in MinIO.
  def sync
    backup
    sm_names = @sm.list.collect { |x| x['path'] }.to_set
    mio_names = @mio.list.collect { |x| '/' + x.key }.to_set
    sm_only = sm_names - mio_names
    mio_only = mio_names - sm_names
    sm_only.each { |x| do_backup x }
    mio_only.each do |x|
      puts "deleting #{x}"
      @mio.delete x[1..-1]
    end
  end

  private

  def do_backup(data)
    # Remove leading slash
    key = data['path'][1..-1]
    data.delete('path')
    r = HTTP.get(data['url'])
    # TODO: Add content type
    puts "putting #{key}"
    @mio.put(
      key: key,
      body: r.to_s,
      metadata: { 'json': JSON.dump(data) }
    )
  end

  def check_and_backup(data)
    # Remove leading slash
    key = data['path'][1..-1]
    print "checking #{key} ... "
    os = @mio.object_summary(key)
    if os.exists?
      puts 'skip'
    else
      do_backup(data)
    end
  end
end

def main
  if ARGV.length > 1
    puts 'Too many arguments'
    return
  elsif ARGV[0] == 'help'
    puts <<~HELP
      Usage: bundle exec app.rb [cmd]

      Available commands:
          - backup      backup SM.MS to S3 bucket
          - diff        show differences between SM.MS and S3 bucket
          - sync        sync SM.MS to S3 bucket (this will delete redundant pictures in bucket)
    HELP
    return
  end

  app = App.new('config.toml')

  case ARGV[0]
  when 'backup'
    app.backup
  when 'diff'
    app.diff
  when 'sync'
    app.sync
  else
    puts 'Invalid command'
  end
end

main
